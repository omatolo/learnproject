﻿using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Dynamic;
using System.IO.Compression;
using System.Reflection.Metadata;
using System.Text.Json;
using VueWithNetCore.Server.Models.Datas;
using VueWithNetCore.Server.Utility;

namespace VueWithNetCore.Server.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LanguageController : ControllerBase
    {
        private readonly ILogger<LanguageController> _logger;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly string jsonFilePath;

        public LanguageController(ILogger<LanguageController> logger, IWebHostEnvironment webHostEnvironment)
        {
            _logger = logger;
            _webHostEnvironment = webHostEnvironment;
            jsonFilePath = Path.Combine(_webHostEnvironment.WebRootPath, "System", "languages.json");
        }

        /// <summary>
        /// 讀取多國語文Json
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetLanguangeData()
        {
            string jsonContent = await System.IO.File.ReadAllTextAsync(jsonFilePath);
            JObject jsonObject = JObject.Parse(jsonContent);

            // 動態取得語言資料
            var languageData = new ExpandoObject() as IDictionary<string, object>;

            foreach (var language in jsonObject)
            {
                var languageName = language.Key;
                var languageValues = language.Value.ToObject<Dictionary<string, string>>();
                languageData[languageName] = languageValues;
            }


            return new JsonResult(languageData);
        }

        /// <summary>
        /// 新增/編輯多國語言
        /// </summary>
        /// <param name="languageData"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> CreateOrUpdate([FromBody] List<LanguageData> languageData)
        {
            try
            {
                JsonTools jsonTools = new JsonTools();

                var jsonObject = await jsonTools.ReadJObjectByPath(jsonFilePath);

                // 迴圈讀取判斷資料新增
                languageData.ForEach(f =>
                {
                    // 修改或新增
                    jsonTools.UpdateJsonField(jsonObject, f.Type, f.Key, f.Value);
                });

                // 將原先JSON備份 建立一個 ZIP 檔 (取名為bak, 避免git上傳)
                var zipFilePath = jsonFilePath.Replace(".json", $"_{DateTime.Now.ToString("yyyyMMddHHmmss")}_A.bak");
                using (ZipArchive archive = ZipFile.Open(zipFilePath, ZipArchiveMode.Create))
                {
                    // 將檔案加入 ZIP 檔
                    archive.CreateEntryFromFile(jsonFilePath, Path.GetFileName(jsonFilePath));
                }

                // 將修改後的 JSON 寫回檔案
                await System.IO.File.WriteAllTextAsync(jsonFilePath, jsonObject.ToString());
            }
            catch (Exception ex)
            {
                var msg = ex;
            }

            return NoContent();
        }

        /// <summary>
        /// 刪除資料
        /// </summary>
        /// <param name="languageData"></param>
        /// <returns></returns>
        [HttpDelete("{langKey}")]
        public async Task<IActionResult> Delete(string langKey)
        {
            try
            {
                JsonTools jsonTools = new JsonTools();

                var jsonObject = await jsonTools.ReadJObjectByPath(jsonFilePath);

                // 資料刪除
                foreach (var item in jsonObject.Properties())
                {
                    var t = jsonObject[item.Name][langKey];

                    if (jsonObject[item.Name] != null)
                        if (jsonObject[item.Name][langKey] != null)
                            jsonObject[item.Name][langKey].Parent.Remove();
                }

                // 將原先JSON備份 建立一個 ZIP 檔 (取名為bak, 避免git上傳)
                var zipFilePath = jsonFilePath.Replace(".json", $"_{DateTime.Now.ToString("yyyyMMddHHmmss")}_D.bak");
                using (ZipArchive archive = ZipFile.Open(zipFilePath, ZipArchiveMode.Create))
                {
                    // 將檔案加入 ZIP 檔
                    archive.CreateEntryFromFile(jsonFilePath, Path.GetFileName(jsonFilePath));
                }

                // 將修改後的 JSON 寫回檔案
                await System.IO.File.WriteAllTextAsync(jsonFilePath, jsonObject.ToString());
            }
            catch (Exception ex)
            {
                var msg = ex;
                return BadRequest();
            }

            return NoContent();
        }
    }
}
