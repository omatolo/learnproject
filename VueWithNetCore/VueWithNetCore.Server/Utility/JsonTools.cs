﻿using Newtonsoft.Json.Linq;

namespace VueWithNetCore.Server.Utility
{
    public class JsonTools
    {
        public async Task<JObject> ReadJObjectByPath(string path)
        {
            // 讀取 JSON 檔案
            string jsonContent = await System.IO.File.ReadAllTextAsync(path);

            // 解析 JSON
            return JObject.Parse(jsonContent);
        }

        /// <summary>
        /// 新增或修改json Value
        /// </summary>
        /// <param name="jsonObject"></param>
        /// <param name="node"></param>
        /// <param name="field"></param>
        /// <param name="newValue"></param>
        public void UpdateJsonField(JObject jsonObject, string node, string field, string newValue)
        {
            // 檢查是否存在指定的節點
            if (jsonObject[node] == null)
                jsonObject[node] = new JObject();

            // 檢查是否存在指定的欄位
            JToken fieldToken = jsonObject.SelectToken($"{node}.{field}");
            if (fieldToken != null)
            {
                // 如果存在，更新欄位的值
                fieldToken.Replace(newValue);
            }
            else
            {
                // 如果不存在，新增欄位並存入值
                jsonObject[node][field] = newValue;
            }
        }
    }
}
