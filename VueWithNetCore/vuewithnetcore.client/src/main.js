import './assets/main.css'
import { createApp } from 'vue';
import App from './App.vue';
import { fetchLanguageData } from './plugins/translations.js';
import { createI18n } from 'vue-i18n';

// Plugins 擴充套件
import { registerPlugins } from '@/plugins'

// 語文api, 讀取完成後才執行
fetchLanguageData().then((i18nLanguages) => {
    // 讀取儲存的語言選擇
    let savedLanguage = localStorage.getItem('language');

    if (savedLanguage === null) {
        savedLanguage = 'zh'; // 預設值
    }

    const i18n = createI18n({
        legacy: false,
        locale: savedLanguage, 
        messages: i18nLanguages,
    });

    const app = createApp(App);

    app.use(i18n);

    registerPlugins(app)

    app.mount('#app');
})
