﻿//import i18nLanguages from '../../public/system//languages.json';

//export default i18nLanguages;

//import axios from 'axios';
//const i18nLanguages = axios.get('/api/language');
//export default i18nLanguages;

import axios from 'axios';

let i18nLanguages = null;
function fetchLanguageData() {
    return axios.get('/api/language')
        .then((response) => {
            i18nLanguages = response.data;
            return i18nLanguages;
        })
        .catch((error) => {
            console.error('Error fetching language data:', error);
            throw error;
        });
}

export { fetchLanguageData };