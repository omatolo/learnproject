import { createRouter, createWebHistory } from 'vue-router';

// 添加路由規則
const routes = [
    {
        name: '首頁', 
        path: '/',
        component: () => import('@/components/Index.vue'),
    },    
    {
        name: '圖表',
        path: '/charts',
        component: () => import('@/views/charts/Index.vue'),
    },
    {
        name: '多國語言編輯',
        path: '/backend/EditLanguage',
        component: () => import('@/views/backend/EditLanguage.vue'),
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;